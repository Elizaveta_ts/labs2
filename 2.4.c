#include <stdio.h>
#include <string.h>
#include <ctype.h>

void exchange(char *, int, int);

int main() 
{
	unsigned char s[80];
	int len, i = 0;
	printf("Enter string, please: \n");
	scanf("%s",s);
	len = strlen(s);
	while(i < len-1)
	{
		if(isdigit(s[i])) //if element is number begin
		{
		int j;
			for(j = i+1; j < len; ++j) 
			{
				if(!isdigit(s[j])) //if element is not number continue
				{
					exchange(s, i, j); //exchange number and letter
					break;
				}
			}

		}
		++i;
	}	
	printf("%s\n", s);
	return 0;
}

void exchange(char *s, int i, int j)
{
	char t;
	t=s[j];
	s[j]=s[i];
	s[i]=t;
}