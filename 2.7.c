#include <string.h>
#include <stdio.h>

int main()
{
	unsigned char buf[256];
	int counts[256] = {0};
	int i=0;

	puts("Enter a string: ");
	fgets(buf, 256, stdin);

	while(buf[i])
		counts[buf[i++]]++;
	for(i=1;i<256;i++)
		if(counts[i] != 0)
			printf("'%c' = %d \n", i, counts[i]);
	return 0;
}